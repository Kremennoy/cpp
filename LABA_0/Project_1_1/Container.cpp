#include "Container.h"
#include "ContainerException.h"

namespace Storages {
    Container::Container(int length, int width, int height, double maxWeight) {
        this->length = length;
        this->width = width;
        this->height = height;
        this->maxWeight = maxWeight;
        this->boxArr.clear();
    }

    void Container::deleteBox(int indexOfBox) {
        if (indexOfBox <= this->boxArr.size() - 1) {
            this->boxArr.erase(this->boxArr.begin() + indexOfBox);
            this->boxArr.shrink_to_fit();
        }
        else {
            throw ContainerException("Invalid index");
        }
    }

    unsigned Container::getBoxCount() {
        return this->boxArr.size();
    }

    long Container::priceOfContainer() {
        long result = 0;
        for (Box temp : this->boxArr) {
            result += temp.getValue();
        }
        return result;
    }

    double Container::weightOfContainer() {
        double result = 0;
        for (Box temp : this->boxArr) {
            result += temp.getWeight();
        }
        return result;
    }

    Box Container::getBox(int indexOfBox) {
        if ((indexOfBox <= this->boxArr.size() - 1) || (indexOfBox > 0)) {
            return this->boxArr[indexOfBox];
        }
        else {
            throw ContainerException("Invalid index of Box");
        }
    }

    unsigned Container::addBox(Box obj) {
        if (obj.getWeight() + this->weightOfContainer() < maxWeight &&
            obj.getLength() + this->getLengthOfAll() <= this->length &&
            obj.getHeight() + this->getHeightOfAll() <= this->height &&
            obj.getWidth() + this->getWidthOfAll() <= this->width) {
            this->boxArr.push_back(obj);
            return this->boxArr.size() - 1;
        }
        else {
            throw ContainerException("Oversized box size");
        }
    }

    std::ostream& operator<<(std::ostream& out, const Container& obj) {
        out << "Container`s boxes:\n";
        for (unsigned i = 0; i < obj.boxArr.size(); i++) {
            out << "[" << i << "]." << obj.boxArr[i] << "\n";
        }
        out << "Container length:" << obj.length << "\n";
        out << "Container width:" << obj.width << "\n";
        out << "Container height:" << obj.height << "\n";
        out << "Container max Weight:" << obj.maxWeight << "\n";
        return out;
    }

    std::istream& operator>>(std::istream& in, Container& obj) {
        for (Box& temp : obj.boxArr) {
            in >> temp;
        }
        in >> obj.length;
        in >> obj.width;
        in >> obj.height;
        in >> obj.maxWeight;
        return in;
    }

    Box& Container::operator[](unsigned index) {
        if (index <= this->boxArr.size() - 1) {
            return this->boxArr[index];
        }
        else {
            throw ContainerException("Invalid index of Box");
        }
    }

    long Container::getLengthOfAll() {
        long result = 0;
        for (Box temp : this->boxArr) {
            result += temp.getLength();
        }
        return result;
    }

    long Container::getWidthOfAll() {
        long result = 0;
        for (Box temp : this->boxArr) {
            result += temp.getWidth();
        }
        return result;
    }

    long Container::getHeightOfAll() {
        long result = 0;
        for (Box temp : this->boxArr) {
            result += temp.getHeight();
        }
        return result;
    }

}