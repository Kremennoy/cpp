#include "box.h"
#include "ContainerException.h"

namespace Storages {
	Box::Box() {
		length = 0;
		width = 0;
		height = 0;
		weight = 0;
		value = 0;
	}
	Box::Box(int length, int width, int height, double weight, int value) {
		if (length < 0 || width < 0 || height < 0 || weight < 0 || value < 0)throw ("adasdads");
		this->length = length;
		this->width = width;
		this->height = height;
		this->weight = weight;
		this->value = value;
	}

	std::istream& operator>>(std::istream& in, Box& obj) {
		in >> obj.length;
		in >> obj.width;
		in >> obj.height;
		in >> obj.weight;
		in >> obj.value;
		return in;
	}

	std::ostream& operator<<(std::ostream& out, const Box& obj) {
		out << "Box:(�����:" << obj.length << ", ������:" << obj.width << ", ������:" << obj.height;
		out << ", ���:" << obj.weight << ", ����:" << obj.value << ")";
		return out;
	}

	int totalValue(Box *boxes, int sizeOfMassWhithBoxes)
	{
		int totalValue = 0;
		for (unsigned i = 0; i < sizeOfMassWhithBoxes; i++) {
			totalValue += boxes[i].value;
		}
		return totalValue;
	}

	bool toAdmit(Box *boxes, int sizeOfMassWhithBoxes, int admitingWidth, int admitingHeight, int admitingLength)
	{
		int totalSizeOfBoxes = 0;
		for (unsigned i = 0; i < boxes[sizeOfMassWhithBoxes].length; i++) {
			totalSizeOfBoxes += boxes[i].height + boxes[i].width + boxes[i].length;
		}
		if (totalSizeOfBoxes <= (admitingHeight + admitingWidth + admitingLength)) {
			return true;
		}
		else {
			return false;
		}
	}

	double maxWeightOfBoxes(Box* boxes, int sizeOfMassWhithBoxes, int maxVolume)
	{
		double maxWeightOfBoxes = 0;
		for (unsigned i = 0; i < boxes[sizeOfMassWhithBoxes].length; i++) {
			if (boxes[i].height * boxes[i].width * boxes[i].length <= maxVolume) {
				maxWeightOfBoxes += boxes[i].weight;
			}
		}
		return maxWeightOfBoxes;
	}

	bool matryoshka(Box *boxes, int sizeOfMassWhithBoxes)
	{
		int temp = 0;
		for (unsigned i = 0; i < boxes[sizeOfMassWhithBoxes].length - 1; i++) {
			for (unsigned j = 0; j < boxes[sizeOfMassWhithBoxes].length - i - 1; j++) {
				if (boxes[j].length > boxes[j + 1].length) {
					temp = boxes[j].length;
					boxes[j].length = boxes[j + 1].length;
					boxes[j + 1].length = temp;
				}
				if (boxes[j].length == boxes[j + 1].length) {
					return false;
				}
			}
		}
		for (unsigned i = 0; i < boxes[sizeOfMassWhithBoxes].length; i++) {
			if (boxes[i].width >= boxes[i + 1].width) {
				return false;
			}
			if (boxes[i].height >= boxes[i + 1].height) {
				return false;
			}
		}
		return true;
	}

	bool equalsOfBoxes(Box box1, Box box2)
	{
		if ((box1.height != box2.height) || (box1.length != box2.length) || (box1.width != box2.width) || (box1.value != box2.value) || (box1.weight != box2.weight)) {
			return false;
		}
		return true;
	}

	int Box::getLength() const {
		return length;
	}

	int Box::getWidth() const {
		return width;
	}

	int Box::getHeight() const {
		return height;
	}

	double Box::getWeight() const {
		return weight;
	}

	int Box::getValue() const {
		return value;
	}

	void Box::setLength(int length) {
		this->length = length;
	}

	void Box::setWidth(int width) {
		this->width = width;
	}

	void Box::setHeight(int height) {
		this->height = height;
	}

	void Box::setWeight(double weight) {
		this->weight = weight;
	}

	void Box::setValue(int value) {
		this->value = value;
	}
}