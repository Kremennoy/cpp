﻿// Project_1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "Storages.h"
#include <iostream>
using namespace Storages;
using namespace std;

int main()
{
    //тесты для боксов

    {
        Box chess(1, 2, 3, 4.0, 5);
        Box chess1(2, 3, 5, 7.3, 1);
        Box chess2(3, 4, 6, 8.1, 4);
        Box chess3(22, 13, 90, 12.2, 1);
        Box boxArr[4] = { chess, chess1, chess2, chess3 };

        cout << totalValue(boxArr, 4) << endl;
    }
    {
        Box chess(1, 2, 3, 5.7, 2);
        Box chess1(101, 3, 5, 5.2, 1);
        Box chess2(3, 4, 6, 8, 4);
        Box chess3(22, 13, 70, 12, 1);
        Box boxArr[4] = { chess, chess1, chess2, chess3 };

        cout << toAdmit(boxArr, 4, 50, 100, 150) << endl;
    }

    {
        Box chess(1, 2, 3, 5.7, 2);
        Box chess1(10, 3, 5, 5.2, 1);
        Box chess2(3, 4, 6, 8, 4);
        Box chess3(22, 13, 70, 12, 1);
        Box boxArr[4] = { chess, chess1, chess2, chess3 };

        cout << maxWeightOfBoxes(boxArr, 4, 100) << endl;
    }
    {
        Box chess(4, 6, 7, 5.7, 2);
        Box chess1(1, 1, 3, 5.2, 1);
        Box chess2(3, 5, 6, 8, 4);
        Box chess3(2, 4, 5, 12, 1);
        Box chess6(5, 3, 8, 10, 4);
        Box chess4(22, 13, 70, 12, 1);
        Box chess5(22, 13, 70, 12, 1);
        Box boxArr[4] = { chess, chess1, chess2, chess3 };
        Box boxArr1[4] = { chess4, chess, chess5, chess1 };
        Box boxArr2[5] = { chess, chess6, chess1, chess2, chess3 };

        cout << matryoshka(boxArr, 4) << endl;
        cout << matryoshka(boxArr1, 4) << endl;
        cout << matryoshka(boxArr2, 5) << endl;
    }
    {
        Box chess(4, 6, 7, 5.7, 2);
        Box chess1(4, 6, 7, 5.7, 3);
        Box chess2(4, 6, 7, 5.7, 2);

        cout << equalsOfBoxes(chess, chess2) << endl;
        cout << equalsOfBoxes(chess, chess1) << endl;
    }

    //тесты для кейсов

    {
        Container container(930, 900, 700, 200);
        container.addBox(Storages::Box(1, 3, 2, 10, 3));
        container.addBox(Storages::Box(2, 3, 4, 11, 10));
        container.addBox(Storages::Box(4, 5, 1, 12, 31));

        container.getBoxCount();

        container.weightOfContainer();

        container.priceOfContainer();

        container.getBox(1).getWidth();
        {
            Container container(930, 900, 700, 200);
            container.addBox(Storages::Box(1, 3, 2, 10, 3));
            container.addBox(Storages::Box(2, 3, 4, 11, 10));
            container.addBox(Storages::Box(4, 5, 1, 12, 31));

            container.deleteBox(1);
            container.getBox(1).getLength();
        }
        {
            Storages::Container container(930, 900, 700, 200);
            container.addBox(Storages::Box(1, 3, 2, 10, 3));

            container[0].getHeight();
        }

    }


}