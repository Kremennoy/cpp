#ifndef TASK0
#define TASK0

#include <string>
#include <exception>

class ContainerException : public std::exception {
private:
    std::string exceptionText;
public:
    explicit ContainerException(std::string error) : exceptionText(move(error)) {};
    const char* what() const noexcept override { return exceptionText.c_str(); }
};


#endif
